package com.aaron.geofencingapp;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

//TODO Add broadcastreciver to ensure network connection is always present

public class DrawerContainer extends AppCompatActivity {
    private SharedPreferences mSharedPreferences;
    private String[] mDrawerItems;
    private ActionBarDrawerToggle mDrawerIconToggle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private AccessToken mAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_drawer_container);

        //Define shared preferences
        mSharedPreferences = getSharedPreferences("Login", Context.MODE_PRIVATE);

        //Reference layout items
        mDrawerItems = getResources().getStringArray(R.array.tab_item_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerListView = (ListView) findViewById(R.id.drawerList);

        //Adds the array to the DrawerListview using the stated layout
        mDrawerListView.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mDrawerItems));

        //Check if recreating destroyed activity
        if (savedInstanceState != null) {
            //used to retain title on screen rotation
            setTitle(savedInstanceState.getCharSequence("title"));
        } else {
            //Start tracking service
            Intent trackingService = new Intent(this, LocationTrackingService.class);
            startService(trackingService);

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.drawerContentFrame, new ProfileTab())
                    .commit();
            mDrawerListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            mDrawerListView.setItemChecked(0, true);
            setTitle(mDrawerItems[0]);
        }

        //Drawer toggle that allows for actionbar to control drawer
        mDrawerIconToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_closed) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
                syncState();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
        };

        //Set the drawer toggle as the drawer listener
        mDrawerLayout.addDrawerListener(mDrawerIconToggle);
        //Enable icons in the actionbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //Syncs toggle state when the OnRestoreInstanceState has been called
        mDrawerIconToggle.syncState();

        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                switch (position) {
                    case 0: {
                        fragmentManager.beginTransaction()
                                .replace(R.id.drawerContentFrame, new ProfileTab())
                                .commit();
                        setTitle(mDrawerItems[position]);
                        break;
                    }
                    case 1: {
                        fragmentManager.beginTransaction()
                                .replace(R.id.drawerContentFrame, new MapTab())
                                .commit();
                        setTitle(mDrawerItems[position]);
                        break;
                    }
                    default: {
                        fragmentManager.beginTransaction()
                                .replace(R.id.drawerContentFrame, new ProfileTab())
                                .commit();
                        break;
                    }
                }

                //Update title to show whats on the tab and close drawer
                mDrawerListView.setItemChecked(position, true);
                mDrawerLayout.closeDrawer(mDrawerListView);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.homescreen_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Pass click event from icon to indicate drawer should open
        if (mDrawerIconToggle.onOptionsItemSelected(item)) {
            return true;
        }
        mAccessToken = AccessToken.getCurrentAccessToken();
        int id = item.getItemId();
        switch (id) {
            //If the logout button is pressed
            case R.id.logout_button:
                //Stops the service when logged out
                stopService(new Intent(DrawerContainer.this, LocationTrackingService.class));
                //Clears any notifications
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancelAll();
                if (mAccessToken != null) {
                    LoginManager.getInstance().logOut();
                    Intent goToLoginScreen = new Intent(this, LoginActivity.class);
                    //Used to wipe the back-stack of activities
                    goToLoginScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(goToLoginScreen);
                    return true;
                } else {
                    //Changes the SharedPreferences to state no one is logged in
                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                    editor.putBoolean("LOGGEDIN", false);
                    editor.apply();
                    Intent goToLoginScreen = new Intent(this, LoginActivity.class);
                    //Used to wipe the back-stack of activities
                    goToLoginScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(goToLoginScreen);
                    return true;
                }
            case R.id.action_settings:
                Intent goToSettings = new Intent(this, Settings.class);
                startActivity(goToSettings);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence("title", getTitle());
    }
}
