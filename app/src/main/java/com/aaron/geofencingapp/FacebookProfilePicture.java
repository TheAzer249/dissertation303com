package com.aaron.geofencingapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

//Async to run in background
public class FacebookProfilePicture extends AsyncTask<String, Void, RoundedBitmapDrawable> {
    private ImageView mImageView;

    public FacebookProfilePicture(ImageView imageView) {
        this.mImageView = imageView;
    }

    @Override
    protected RoundedBitmapDrawable doInBackground(String... urls) {
        RoundedBitmapDrawable circleFacebookPicture = null;
        try {
            //Gets the bitmap from the URL
            InputStream in = new java.net.URL(urls[0]).openStream();
            Bitmap imageDownloaded = BitmapFactory.decodeStream(in);
            //Turns the bitmap into a RoundedBitmapDrawable
            circleFacebookPicture = RoundedBitmapDrawableFactory.create(null, imageDownloaded);
            circleFacebookPicture.setCircular(true);
        } catch (Exception e) {
            Log.e("FacebookProfilePicture:", e.getMessage());
        }
        return circleFacebookPicture;
    }

    //After executing sets the image to imageview
    @Override
    protected void onPostExecute(RoundedBitmapDrawable finalPicture) {
        mImageView.setImageDrawable(finalPicture);
    }
}