package com.aaron.geofencingapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

public class MapTab extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mMap;
    private DatabaseHandler mdb;
    private GeofenceBuilder mGeofenceBuilder;
    private SharedPreferences mSharedPreferences;
    private FloatingActionButton mAddGeofence;
    private FloatingActionButton mGoToMe;
    private GoogleApiClient mGoogleApiClient;
    private String mUsername;

    public MapTab() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mdb = new DatabaseHandler(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map_tab, container, false);

        mSharedPreferences = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
        AccessToken accessToken = AccessToken.getCurrentAccessToken();

        //Find views on the fragment
        mSupportMapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
        mSupportMapFragment.getMapAsync(this);
        mAddGeofence = (FloatingActionButton) v.findViewById(R.id.addGeofence);
        mGoToMe = (FloatingActionButton) v.findViewById(R.id.goToMe);

        //Check if logged in with facebook or not
        if (accessToken != null) {
            mUsername = accessToken.getUserId();
        } else {
            mUsername = mSharedPreferences.getString("USERNAME", "Error");
        }

        //Connecting to GoogleApiClient as its used for getting detailed information such as last known location
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(MapTab.this)
                    .addOnConnectionFailedListener(MapTab.this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }

        //Listener for floatingactionbutton, user for going to current location of user
        mGoToMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Location userLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                //Permissions check to stop crashing if location permissions have not been given
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    //Go to the last known location, if statement stops crashing if there is no location data, (Stops NPE)
                    if (userLocation != null) {
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(userLocation.getLatitude(), userLocation.getLongitude()), 17);
                        mMap.animateCamera(cameraUpdate);
                    }
                } else {
                    Toast.makeText(getActivity(), R.string.permissions_not_given, Toast.LENGTH_LONG).show();
                    Log.e("Map Error", getText(R.string.permissions_not_given).toString());
                }
            }
        });

        //Listener for floatingactionbutton, used for adding new geofences
        mAddGeofence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAddGeofence.hide();
                mGoToMe.hide();
                LayoutInflater addGeofenceForm = getActivity().getLayoutInflater();
                final AlertDialog.Builder addPlaceBuilder = new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.add_geofence)
                        .setIcon(R.mipmap.ic_launcher)
                        .setView(addGeofenceForm.inflate(R.layout.add_geofence_layout, null))
                        .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mAddGeofence.show();
                                mGoToMe.show();
                            }
                        });
                final AlertDialog addPlaceDialog = addPlaceBuilder.create();
                //Listener is added to the dialog to allow for compatibility with API 15
                //On dismiss listeners are not available in the dialog for bellow API 17
                addPlaceDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        mAddGeofence.show();
                        mGoToMe.show();
                    }
                });

                addPlaceDialog.show();

                //TODO also need to implement a check to not allow more than 7 decimal places
                //Used to add a geofence to the database if it goes through all checks
                addPlaceDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText addPlaceName = (EditText) addPlaceDialog.findViewById(R.id.addPlaceName);
                        EditText addPlaceLat = (EditText) addPlaceDialog.findViewById(R.id.addLatitude);
                        EditText addPlaceLong = (EditText) addPlaceDialog.findViewById(R.id.addLongitude);
                        EditText addPlaceRadius = (EditText) addPlaceDialog.findViewById(R.id.addRadius);
                        if (addPlaceName.length() > 0 && addPlaceLat.length() > 0 && addPlaceLong.length() > 0 && addPlaceRadius.length() > 0) { //Are fields empty
                            if (!mdb.checkValueExists("geofencePlaces", "place", addPlaceName.getText().toString())) { //Has the name for the geofence been used
                                double lat = Double.parseDouble(addPlaceLat.getText().toString());
                                double lang = Double.parseDouble(addPlaceLong.getText().toString());
                                if (lat <= 90 && lat >= -90) { //If a valid latitude has been entered
                                    if (lang <= 180 && lang >= -180) {
                                        Boolean success = mdb.addGeofence(new GeofenceObject(addPlaceName.getText().toString(), //If not add the geofence
                                                lat,
                                                lang,
                                                Float.parseFloat(addPlaceRadius.getText().toString()),
                                                mUsername));

                                        //Show the floating action buttons again
                                        mAddGeofence.show();
                                        mGoToMe.show();

                                        //Quick check to see if the geofences have been added correctly, feedback to the user
                                        if (success)
                                            Toast.makeText(getActivity(), R.string.geofence_added, Toast.LENGTH_SHORT).show();
                                        else
                                            Toast.makeText(getActivity(), R.string.error_adding_geofence, Toast.LENGTH_SHORT).show();

                                        //Adds new geofences to map after clearing all previous markers
                                        mMap.clear();
                                        mGeofenceBuilder = new GeofenceBuilder(mMap, getActivity());
                                        if (mdb.getStringValue("userSettings", mUsername, "geofenceOnMap").equals("1")) {
                                            mGeofenceBuilder.addGeofencesToMap();
                                        }

                                        //starts and stops the service
                                        Intent trackingService = new Intent(getActivity(), LocationTrackingService.class);
                                        getActivity().stopService(trackingService);
                                        getActivity().startService(trackingService);
                                        addPlaceDialog.dismiss();
                                    } else {
                                        addPlaceLong.setText("");
                                        addPlaceLong.setError(getText(R.string.valid_long));
                                    }
                                } else {
                                    addPlaceLat.setText("");
                                    addPlaceLat.setError(getText(R.string.valid_lat));
                                }
                            } else {
                                addPlaceName.setText("");
                                addPlaceName.setError(getText(R.string.geofence_name_already_exists));
                            }
                        } else {
                            if (addPlaceName.getText().length() < 1)
                                addPlaceName.setError(getText(R.string.fields_empty));
                            if (addPlaceLat.getText().length() < 1)
                                addPlaceLat.setError(getText(R.string.fields_empty));
                            if (addPlaceLong.getText().length() < 1)
                                addPlaceLong.setError(getText(R.string.fields_empty));
                            if (addPlaceRadius.getText().length() < 1)
                                addPlaceRadius.setError(getText(R.string.fields_empty));
                        }
                    }
                });
            }
        });
        return v;
    }

    //Used as a callback to getmapasync. Sets up the map when it is ready.
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;

        //Builds the geofences onto the map
        mGeofenceBuilder = new GeofenceBuilder(mMap, getActivity());
        if (mdb.getStringValue("userSettings", mUsername, "geofenceOnMap").equals("1")) {
            mGeofenceBuilder.addGeofencesToMap();
        }

        //Permissions check to stop crashing if location permissions have not been given
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //Show location on the map, but don't show the UI element to center camera, will be controlled by FAB
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setMapToolbarEnabled(false);
        } else {
            Toast.makeText(getActivity(), R.string.permissions_not_given, Toast.LENGTH_LONG).show();
            Log.e("Map Error", getText(R.string.permissions_not_given).toString());
        }
    }

    //Used for the googleApiClient
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("MapGoogleApiClient", "Connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("MapGoogleApiClient", "Suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("MapGoogleApiClient", "Failed");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //Disconnect from client on destroy
        mGoogleApiClient.disconnect();
    }
}
