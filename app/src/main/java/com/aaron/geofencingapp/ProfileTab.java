package com.aaron.geofencingapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ProfileTab extends Fragment {
    private LinearLayout mInteractionHost;
    private SharedPreferences mSharedPreferences;
    private String mUsername;
    private DatabaseHandler mdb;
    private ImageView mProfileImage;
    private TextView mUsernameTextview;
    private TextView mFullName;
    private AccessToken mAccessToken;

    public ProfileTab() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mdb = new DatabaseHandler(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_tab, container, false);

        //References items in XML file
        mSharedPreferences = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
        mProfileImage = (ImageView) v.findViewById(R.id.profileImage);
        mUsernameTextview = (TextView) v.findViewById(R.id.profileUsername);
        mFullName = (TextView) v.findViewById(R.id.profileName);
        mInteractionHost = (LinearLayout) v.findViewById(R.id.interactionContainer);
        mAccessToken = AccessToken.getCurrentAccessToken();

        try {
            if (mAccessToken != null) {
                mUsername = mAccessToken.getUserId();
                populateProfileFacebook();
            } else {
                mUsername = mSharedPreferences.getString("USERNAME", "Error");
                populateProfileLocal(mdb.getStringValue("userInformation", mUsernameTextview.getText().toString(), "profilePicture"));
            }
        } finally {
            List<InteractionObject> mInteractionObjectList;
            mInteractionObjectList = mdb.getInteractions(mUsername);
            populateInteractions(mInteractionObjectList);
        }

        mProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAccessToken == null) {
                    //TODO implement local profile picture options this can be done later on though
                }
            }
        });

        return v;
    }

    public void populateProfileFacebook() {
        //Facebook graph request
        GraphRequest request = GraphRequest.newMeRequest(mAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    //Getting needed information from object
                    String facebookId = object.getString("id");
                    String facebookName = object.getString("name");
                    //Setting TextView to name
                    mUsernameTextview.setText(facebookId);
                    mFullName.setText(facebookName);
                    //Get facebook profile picture in large format
                    new FacebookProfilePicture(mProfileImage).execute("https://graph.facebook.com/" + facebookId + "/picture?height=400&width=400");
                } catch (JSONException exception) {
                    throw new RuntimeException(exception);
                }
            }
        });
        Bundle fields = new Bundle();
        fields.putString("fields", "id,name");
        request.setParameters(fields);
        request.executeAsync();
    }

    public void populateProfileLocal(String profilePictureLocation) {
        if (profilePictureLocation == null) { //If the user has no profile picture
            new LocalProfilePicture(getContext(), profilePictureLocation, mProfileImage).execute();
        }
        mUsernameTextview.setText(mSharedPreferences.getString("USERNAME", "Error"));
        mFullName.setText(mdb.getStringValue("userInformation", mUsername, "name"));
    }

    public void populateInteractions(List<InteractionObject> interactionObjects) {
        //For every interaction in interactionObjects list
        for (InteractionObject interactionObject : interactionObjects) {
            String place = interactionObject.getmPlace();
            String time = interactionObject.getmTime();
            String googlePlacesName = interactionObject.getmGooglePlaceName();
            String googlePlacesId = interactionObject.getmGooglePlaceId();

            //Pass the interactionObject values into the fragment
            Bundle data = new Bundle();
            data.putString("place", place);
            data.putString("time", time);
            data.putString("googlePlaceName", googlePlacesName);
            data.putString("googlePlaceId", googlePlacesId);

            //Make a new fragment for the interactionObject so it can be placed in scrollview
            Fragment interactionDetailsFragment = new InteractionDetailsFragment();
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            interactionDetailsFragment.setArguments(data);
            transaction.add(R.id.interactionContainer,
                    interactionDetailsFragment,
                    time);
            transaction.commit();
        }
    }
}
