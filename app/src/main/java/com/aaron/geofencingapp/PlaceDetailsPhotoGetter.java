package com.aaron.geofencingapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;


public class PlaceDetailsPhotoGetter extends AsyncTask<String, Void, Void> implements GoogleApiClient.ConnectionCallbacks {

    private GoogleApiClient mGoogleApiClient;
    private LinearLayout mPhotoLayout;
    private HorizontalScrollView mPhotoScrollView;
    private String mPlaceId;
    private Context mContext;
    private List<GoogleImage> photoList = new ArrayList<>();
    private List<CharSequence> attributionList = new ArrayList<>();

    public PlaceDetailsPhotoGetter(LinearLayout photoLayout, String placeId, Context context, HorizontalScrollView horizontalScrollView) {
        this.mPhotoLayout = photoLayout;
        this.mPlaceId = placeId;
        this.mContext = context;
        this.mPhotoScrollView = horizontalScrollView;
    }

    @Override
    protected Void doInBackground(String... params) {

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .addConnectionCallbacks(this)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
            mGoogleApiClient.connect();
        }

        PlacePhotoMetadataResult photoData = Places.GeoDataApi.getPlacePhotos(mGoogleApiClient, mPlaceId).await();
        if (photoData != null && photoData.getStatus().isSuccess()) {
            //Get the data in a buffer to be manipulated
            PlacePhotoMetadataBuffer photoMetadataBuffer = photoData.getPhotoMetadata();
            for (PlacePhotoMetadata placePhoto : photoMetadataBuffer) {
                //Filters out low quality pictures
                if (placePhoto.getMaxHeight() >= 400) {
                    Bitmap photoBitmap = placePhoto.getScaledPhoto(mGoogleApiClient, 600, 600)
                            .await()
                            .getBitmap();
                    //This needs to be used to comply with the Google Api usage policy
                    CharSequence attribution = placePhoto.getAttributions();
                    //Creates a new photo/bitmap pair and adds them to a list
                    photoList.add(new GoogleImage(photoBitmap, attribution));
                    //Adds attribution to list
                    attributionList.add(attribution);
                }
            }
            photoMetadataBuffer.release();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (!photoList.isEmpty()) {
            //Place the images in the banner
            int counter = 0;
            for (final GoogleImage googleImage : photoList) {
                mPhotoScrollView.setVisibility(View.VISIBLE);
                ImageView image = new ImageView(mContext);
                //Setting the tag so the listener can use this
                image.setTag(googleImage.getAttribution());
                image.setImageBitmap(googleImage.getImage());
                //Scale the image to fit nicely in the banner
                image.setScaleType(ImageView.ScaleType.CENTER_CROP);
                mPhotoLayout.addView(image);

                //Used to display photo attribution on click
                final int finalCounter = counter;
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(mContext, Html.fromHtml(attributionList.get(finalCounter).toString()),Toast.LENGTH_LONG).show();
                    }
                });

                counter++;
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    //Class used to make an object of google image, used for attribution
    class GoogleImage{
        private Bitmap mImage;
        private CharSequence mAttribution;

        public GoogleImage(Bitmap image, CharSequence attribution){
            this.mImage = image;
            this.mAttribution = attribution;
        }

        public Bitmap getImage() {
            return mImage;
        }

        public CharSequence getAttribution() {
            return mAttribution;
        }
    }
}
