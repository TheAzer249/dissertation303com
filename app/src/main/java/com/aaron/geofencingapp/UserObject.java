package com.aaron.geofencingapp;

public class UserObject {
    private String mName;
    private String mUsername;
    private String mPassword;
    private String mProfilePicture;

    public UserObject(String name, String username, String password, String mProfilePicture) {
        this.mName = name;
        this.mUsername = username;
        this.mPassword = password;
        this.mProfilePicture = mProfilePicture;
    }

    //Getters

    public String getmName() {
        return mName;
    }

    public String getmUsername() {
        return mUsername;
    }

    public String getmPassword() {
        return mPassword;
    }

    public String getmProfilePicture() {
        return mProfilePicture;
    }
}

