package com.aaron.geofencingapp;

import android.Manifest;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.facebook.AccessToken;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class GeofenceInteractionService extends IntentService implements GoogleApiClient.ConnectionCallbacks {
    private GoogleApiClient mGoogleApiClient;
    private DatabaseHandler mdb;
    private String mUsername;
    private SharedPreferences mSharedPreferences;

    private GeofencingEvent mGeofencingEvent;
    private int mInteractionType;
    private List<Geofence> mTriggeringGeofences;
    private String mDateAndTime;
    private String mGooglePlaceID;
    private String mGooglePlaceName;

    public GeofenceInteractionService() {
        //Used to name the worker thread
        super("GeofenceInteractions");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mGeofencingEvent = GeofencingEvent.fromIntent(intent);
        mInteractionType = mGeofencingEvent.getGeofenceTransition();
        mTriggeringGeofences = mGeofencingEvent.getTriggeringGeofences();
        mdb = new DatabaseHandler(this);

        //Get current username of the person logged in, or facebook id
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        mSharedPreferences = getSharedPreferences("Login", Context.MODE_PRIVATE);
        if (accessToken != null) {
            mUsername = accessToken.getUserId();
        } else {
            mUsername = mSharedPreferences.getString("USERNAME", "Error");
        }

        //Gets the current date and time for the interaction
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dmyFormat = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH);
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss", Locale.ENGLISH);
        String dmy = dmyFormat.format(calendar.getTime());
        String time = timeFormat.format(calendar.getTime());
        mDateAndTime = dmy + " at " + time;

        //Connecting to GoogleApiClient for Geo_Data_API and Place_Detection_Api
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    //When the GoogleApiClient is connected handle the intent
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //If there is an error with the geofencing event
        if (mGeofencingEvent.hasError()) {
            Log.e("GeofencingService", "Error with geofecing event");
        }

        //Not going to be using enter a lot, using dwell instead as it prevents notification spam
        if (mInteractionType == Geofence.GEOFENCE_TRANSITION_ENTER) {
            Log.d("InteractionObject:", "Enter!");
        } else if (mInteractionType == Geofence.GEOFENCE_TRANSITION_EXIT) {
            Log.d("InteractionObject:", "Exit!");
        } else if (mInteractionType == Geofence.GEOFENCE_TRANSITION_DWELL) {
            Log.d("InteractionObject:", "Dwell!");

            //Used to get the suspected place the phone is at using google places api
            getPlaceGooglePlacesApi();
        }
    }

    public void getPlaceGooglePlacesApi() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            //Gets possible list of current places with confidence value
            PendingResult<PlaceLikelihoodBuffer> pendingResult = Places
                    .PlaceDetectionApi
                    .getCurrentPlace(mGoogleApiClient, null);

            //Callback after the request to get the places that could be where the user is
            pendingResult.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(@NonNull PlaceLikelihoodBuffer placeLikelihoods) {
                    mGooglePlaceName = placeLikelihoods.get(0).getPlace().getName().toString();
                    mGooglePlaceID = placeLikelihoods.get(0).getPlace().getId();

                    //For every possible place that is found
                    for (PlaceLikelihood placeLikelihood : placeLikelihoods) {
                        Log.d("GooglePlaces", "Place " + placeLikelihood.getPlace().getName() + " Possibility " + placeLikelihood.getLikelihood());
                    }

                    //Used to add the interaction to the local database so it can be loaded in the local manager
                    //This is done when the result callback is activated to stop the place being equal to null
                    addInteractionToDatabase();

                    if (mdb.getStringValue("userSettings", mUsername, "notifications").equals("1")) {
                        createNotification();
                    }

                    placeLikelihoods.release();
                }
            });
        }
    }

    //Used to send a notification that a fence has been triggered
    private void createNotification() {
        int notificationId;
        NotificationCompat.Builder notificationBuilder;
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (mInteractionType == Geofence.GEOFENCE_TRANSITION_DWELL) {
            int i = 0;
            for (Geofence geofence : mTriggeringGeofences) {
                notificationId = i;

                //Creates an intent that moves to the drawerContainer
                Intent goToInfo = new Intent(this, PlaceDetails.class);
                goToInfo.putExtra("Notification", true);
                goToInfo.putExtra("PlaceId", mGooglePlaceID);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        this,
                        0,
                        goToInfo,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

                //Builds a notification for the interaction and displays it to the user
                notificationBuilder = new NotificationCompat.Builder(this)
                        .setAutoCancel(true)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("You are near " + mGooglePlaceName)
                        .setContentText(getText(R.string.more_information))
                        .setContentIntent(pendingIntent);
                notificationManager.notify(notificationId, notificationBuilder.build());
                i++;
            }
        }
    }

    //Used to add a interaction to the database
    private void addInteractionToDatabase() {
        int i = 0;
        for (Geofence geofence : mTriggeringGeofences) {
            //Adds the interaction to the database
            mdb.addInteraction(new InteractionObject(mUsername,
                    mTriggeringGeofences.get(i).getRequestId(),
                    mDateAndTime, mGooglePlaceName, mGooglePlaceID));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
