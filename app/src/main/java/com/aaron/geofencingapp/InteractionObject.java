package com.aaron.geofencingapp;

public class InteractionObject {
    private String mPlace;
    private String mTime;
    private String mUsername;
    private String mGooglePlaceName;
    private String mGooglePlaceId;

    public InteractionObject(String username, String place, String time, String googlePlaceName, String googlePlaceId) {
        this.mPlace = place;
        this.mTime = time;
        this.mUsername = username;
        this.mGooglePlaceName = googlePlaceName;
        this.mGooglePlaceId = googlePlaceId ;
    }

    public String getmPlace() {
        return mPlace;
    }

    public String getmTime() {
        return mTime;
    }

    public String getmUsername() {
        return mUsername;
    }

    public String getmGooglePlaceName() {
        return mGooglePlaceName;
    }

    public String getmGooglePlaceId() {
        return mGooglePlaceId;
    }

    public void setmPlace(String place) {
        mPlace = place;
    }

    public void setmTime(String time) {
        mTime = time;
    }

    public void setmUsername(String username) {
        mUsername = username;
    }
}