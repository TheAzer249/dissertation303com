package com.aaron.geofencingapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;

public class Settings extends AppCompatActivity {
    private SharedPreferences mSharedPreferences;
    private DatabaseHandler mdb;
    private String mUsername;
    private TextView mDeleteGeofence;
    private TextView mDeleteAllGeofences;
    private Switch mGeofenceOnMap;
    private Switch mNotifications;
    private Intent mTrackingService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mdb = new DatabaseHandler(this);
        mTrackingService = new Intent(Settings.this, LocationTrackingService.class);
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        mSharedPreferences = getSharedPreferences("Login", Context.MODE_PRIVATE);
        mDeleteGeofence = (TextView) findViewById(R.id.deleteGeofence);
        mDeleteAllGeofences = (TextView) findViewById(R.id.deleteAllGeofence);
        mGeofenceOnMap = (Switch) findViewById(R.id.switchOnMap);
        mNotifications = (Switch) findViewById(R.id.switchNotifications);

        //Check if logged in with facebook or not
        if (accessToken != null) {
            mUsername = accessToken.getUserId();
        } else {
            mUsername = mSharedPreferences.getString("USERNAME", "Error");
        }

        //Check settings and set them correctly
        checkSettings();

        //Listener for the GeofenceOnMap switch
        mGeofenceOnMap.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mdb.updateStringValue("userSettings", mUsername, "geofenceOnMap", "1");
                } else {
                    mdb.updateStringValue("userSettings", mUsername, "geofenceOnMap", "0");
                }
            }
        });

        mNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mdb.updateStringValue("userSettings", mUsername, "notifications", "1");
                } else {
                    mdb.updateStringValue("userSettings", mUsername, "notifications", "0");
                }
            }
        });

        //Listener to check when the option to delete all geofences is clicked
        mDeleteAllGeofences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder connectionIssues = new AlertDialog.Builder(Settings.this)
                        .setTitle(R.string.delete_all_geofence)
                        .setIcon(R.mipmap.ic_launcher)
                        .setNegativeButton(R.string.no, null)
                        .setMessage(R.string.are_you_sure_delete_all_geofence)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mdb.deleteAllGeofence(mUsername);
                                stopService(mTrackingService);
                                startService(mTrackingService);
                                Toast.makeText(Settings.this, R.string.geofences_removed, Toast.LENGTH_SHORT).show();
                            }
                        });
                connectionIssues.show(); //Shows the dialog
            }
        });

        //Listener to check for when the textview to remove geofences is pressed
        mDeleteGeofence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO add something to delete geofences
                LayoutInflater removeGeofenceForm = getLayoutInflater();
                final AlertDialog.Builder removeGeofenceBuilder = new AlertDialog.Builder(Settings.this)
                        .setTitle(R.string.delete_geofence)
                        .setIcon(R.mipmap.ic_launcher)
                        .setView(removeGeofenceForm.inflate(R.layout.remove_geofence_layout, null))
                        .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                final AlertDialog removeGeofenceDialog = removeGeofenceBuilder.create();
                removeGeofenceDialog.show();

                removeGeofenceDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText place = (EditText) removeGeofenceDialog.findViewById(R.id.removePlaceName);
                        if (mdb.checkGeofenceExists(place.getText().toString(), mUsername)) {
                            mdb.deleteGeofence(place.getText().toString(), mUsername);
                            //TODO again, needs to be changed later down for line but for now this de-registers and re-registers geofences
                            stopService(mTrackingService);
                            startService(mTrackingService);
                            removeGeofenceDialog.dismiss();
                            Toast.makeText(Settings.this, R.string.geofence_removed, Toast.LENGTH_LONG).show();
                        } else {
                            place.setText("");
                            place.setError(getText(R.string.geofence_not_found));
                        }
                    }
                });
            }
        });
    }

    //Used to control the back button on the action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void checkSettings() {
        //Check to see if the geofences should be displayed on the map
        if (mdb.getStringValue("userSettings", mUsername, "geofenceOnMap").equals("1")) {
            mGeofenceOnMap.setChecked(true);
        } else {
            mGeofenceOnMap.setChecked(false);
        }

        //Checks to see if notifications are enabled
        if (mdb.getStringValue("userSettings", mUsername, "notifications").equals("1")) {
            mNotifications.setChecked(true);
        } else {
            mNotifications.setChecked(false);
        }
    }

    //TODO possibly change this as it's not good practice, up and back 'should' be different
    //Used to force the back button to act in a similar way to up navigation
    //This will allow for settings to take effect straight away no matter what nav button is pressed
    @Override
    public void onBackPressed() {
        Intent goToDrawerContainer = new Intent(this, DrawerContainer.class);
        goToDrawerContainer.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(goToDrawerContainer);
    }
}
