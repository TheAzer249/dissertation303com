//TODO Implement Facebook integration, Possibility of disregarding local accounts all together, this could also be done through the use of a Google account
//TODO account for dialog vanishing on orientation change
package com.aaron.geofencingapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

public class LoginActivity extends AppCompatActivity {
    //Permission case constant, used for callback
    private static final int LOCATION_PERMISSION_REQUEST = 0;
    //Member variable definitions
    private EditText mLoginUsername;
    private EditText mLoginPassword;
    private Button mLoginButton;
    private TextView mRegisterButton;
    private DatabaseHandler mdb;
    private SharedPreferences mSharedPreferences;
    private CallbackManager mCallbackManager;
    private AccessToken mAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Initialize the facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        mdb = new DatabaseHandler(this);

        //Reference layout items
        mLoginUsername = (EditText) findViewById(R.id.input_username);
        mLoginPassword = (EditText) findViewById(R.id.input_password);
        mLoginButton = (Button) findViewById(R.id.loginButton);
        mRegisterButton = (TextView) findViewById(R.id.registerButton);

        //Allows the hint to maintain normal formatting and retain starred out
        mLoginPassword.setTypeface(Typeface.DEFAULT);
        mLoginPassword.setTransformationMethod(new PasswordTransformationMethod());

        //Define shared preferences
        mSharedPreferences = getSharedPreferences("Login", Context.MODE_PRIVATE);

        //If a user is logged in to facebook, or locally go to the homescreen
        mAccessToken = AccessToken.getCurrentAccessToken();
        if (mSharedPreferences.getBoolean("LOGGEDIN", false) |
                mAccessToken != null) {
            loginUser();
        }

        //Callback manager used for facebook login button
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Add user settings to database
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                //If the account has been used before, theres no need to make settings
                if (!mdb.checkValueExists("userSettings", "username", accessToken.getUserId())) {
                    mdb.addFacebookUserSettings(accessToken.getUserId());
                }
                loginUser();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {

            }
        });

        //Listener for the login button - Hints are refreshed and used to indicate when details are wrong
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginUsername.setHint(R.string.username);
                mLoginPassword.setHint(R.string.password);
                if (mdb.checkValueExists("userInformation", "username", mLoginUsername.getText().toString())) {
                    if (mdb.checkLoginDetails(mLoginUsername.getText().toString(), mLoginPassword.getText().toString())) {
                        //Adds the login status to Sharedpreferences along with the currently logged in username
                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                        editor.putBoolean("LOGGEDIN", true);
                        editor.putString("USERNAME", mLoginUsername.getText().toString());
                        editor.apply();
                        Log.d("Login:", "Valid details entered.");
                        loginUser(); // Used to login the user, calls the method
                    } else {
                        mLoginPassword.setError(getText(R.string.invalid_password));
                    }
                } else {
                    mLoginUsername.setError(getText(R.string.invalid_username));
                }
            }
        });

        //Listener for the register button
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Used to create a register form inside of an alert dialog
                LayoutInflater registerForm = getLayoutInflater();
                final AlertDialog.Builder registerBuilder = new AlertDialog.Builder(LoginActivity.this)
                        .setTitle(R.string.register)
                        .setIcon(R.mipmap.ic_launcher)
                        .setView(registerForm.inflate(R.layout.register_layout, null))
                        .setPositiveButton(R.string.register, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        })
                        .setNegativeButton(R.string.cancel, null);
                final AlertDialog registerDialog = registerBuilder.create();
                registerDialog.show();

                //Used to override the positive button so it does not close when the register button is pressed
                //Unless there is a valid entry in the registration form
                registerDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Using registerDialog.findView... to pull information from inflated dialog
                        EditText RegisterName = (EditText) registerDialog.findViewById(R.id.registerRealName);
                        EditText RegisterUsername = (EditText) registerDialog.findViewById(R.id.registerUsername);
                        EditText RegisterPassword = (EditText) registerDialog.findViewById(R.id.registerPassword);
                        EditText RegisterPasswordAgain = (EditText) registerDialog.findViewById(R.id.registerPasswordAgain);
                        //Gets information from text views as string
                        String username = RegisterUsername.getText().toString();
                        String password = RegisterPassword.getText().toString();
                        String passwordAgain = RegisterPasswordAgain.getText().toString();
                        String name = RegisterName.getText().toString();
                        //Checks to see if the register form is filled in correctly
                        if (RegisterUsername.length() > 0 && RegisterPassword.length() > 0 && RegisterPasswordAgain.length() > 0 && RegisterName.length() > 0) {
                            if (password.equals(passwordAgain)) {
                                //Register the user using the method in the database handler
                                if (!mdb.checkValueExists("userInformation", "username", username)) {
                                    Boolean success = mdb.addUser(new UserObject(name, username, password, null));
                                    if (!success)
                                        Toast.makeText(getApplicationContext(), R.string.problem_adding_user, Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(getApplicationContext(), R.string.user_added, Toast.LENGTH_SHORT).show();
                                    registerDialog.dismiss();
                                } else {
                                    Log.d("Register form", "The username already exists");
                                    RegisterUsername.setText("");
                                    RegisterUsername.setError(getText(R.string.username_already_exists));
                                }
                            } else {
                                Log.d("Register form", "The passwords do not match");
                                RegisterPassword.setText("");
                                RegisterPassword.setError(getText(R.string.passwords_do_not_match));
                                RegisterPasswordAgain.setText("");
                                RegisterPasswordAgain.setError(getText(R.string.passwords_do_not_match));
                            }
                        } else {
                            Log.d("Register Form", "One of the fields in the register form is empty");
                            if (RegisterUsername.length() < 1)
                                RegisterUsername.setError(getText(R.string.fields_empty));
                            if (RegisterPassword.length() < 1)
                                RegisterPassword.setError(getText(R.string.fields_empty));
                            if (RegisterPasswordAgain.length() < 1)
                                RegisterPasswordAgain.setError(getText(R.string.fields_empty));
                            if (RegisterName.length() < 1)
                                RegisterName.setError(getText(R.string.fields_empty));
                        }
                    }
                });
            }
        });
    }

    //Used to log a user in when the login button is pressed and the correct values are entered
    public void loginUser() {
        //If a user is logged in already but there is no settings for them, make one (this can happen when the app is re-installed)
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null &&
                !mdb.checkValueExists("userSettings", "username", accessToken.getUserId())) {
            mdb.addFacebookUserSettings(accessToken.getUserId());
        }
        //This is used to check if the app has relevant permissions before moving to the main screen.
        //This is because the app requires permissions to use location for Geofencing to work
        if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //Ask for the location permissions if they are not granted
            Log.d("LocationPermReq", "Permission not granted, asking for permission");
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST);
        } else {
            Intent goToHomeScreen = new Intent(this, DrawerContainer.class);
            startActivity(goToHomeScreen);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Used for facebook to log install and activate events
        //Will be reflected on developer dashboard on facebook
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Used for facebook to log deactivate app events
        AppEventsLogger.deactivateApp(this);
    }

    //Should be the same on any login/share activity, forwards onActivityResult to callback manager
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    //Used to handle permission requests
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST:
                //Array is empty if the request is canceled
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent goToHomeScreen = new Intent(this, DrawerContainer.class);
                    startActivity(goToHomeScreen);
                    finish();
                    Log.d("LocationPermReq", "Granted");
                } else {
                    Log.d("LocationPermReq", "Denied");
                    Toast.makeText(this, R.string.allow_location, Toast.LENGTH_SHORT).show();

                    //Logs out of Facebook because permission was not given, this resets the state of the login button
                    mAccessToken = AccessToken.getCurrentAccessToken();
                    if (mAccessToken != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
                return;
        }
    }
}
