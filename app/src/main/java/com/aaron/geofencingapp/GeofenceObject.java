package com.aaron.geofencingapp;

public class GeofenceObject {
    private String mPlace;
    private Double mLat;
    private Double mLong;
    private Float mRadius;
    private String mUsername;

    public GeofenceObject(String place, Double lat, Double longi, Float radius, String username) {
        this.mPlace = place;
        this.mLat = lat;
        this.mLong = longi;
        this.mRadius = radius;
        this.mUsername = username;
    }

    public String getmPlace() {
        return mPlace;
    }

    public Double getmLat() {
        return mLat;
    }

    public Double getmLong() {
        return mLong;
    }

    public Float getmRadius() {
        return mRadius;
    }

    public String getmUsername() {
        return mUsername;
    }
}
