package com.aaron.geofencingapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

public class LocalProfilePicture extends AsyncTask<String, Void, RoundedBitmapDrawable> {
    private Context mContext;
    private String mImagePath;
    private ImageView mImageView;

    //Constructor to make run in background
    public LocalProfilePicture(Context context, String imagePath, ImageView imageView){
        this.mContext = context;
        this.mImagePath = imagePath;
        this.mImageView = imageView;
    }

    @Override
    protected RoundedBitmapDrawable doInBackground(String... params) {
        RoundedBitmapDrawable circleProfilePicture = null;
        if (mImagePath == null) {
            //Decode the blank profile icon and change it to a rounded one
            Bitmap profilePictureBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_profilepicture);
            //Sets the decoded bitmap into a CircularDrawable
            circleProfilePicture = RoundedBitmapDrawableFactory.create(mContext.getResources(), profilePictureBitmap);
            circleProfilePicture.setCircular(true);
        }
        return circleProfilePicture;
    }

    @Override
    protected void onPostExecute(RoundedBitmapDrawable finalPicture) {
        mImageView.setImageDrawable(finalPicture);
    }
}
