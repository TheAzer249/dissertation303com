package com.aaron.geofencingapp;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class GeofenceBuilder implements ResultCallback<Status> {
    //TODO change these three lists to one geofence object, this applies to in the databasehandler
    private List<GeofenceObject> mAllFetchedGeofenceObjects;
    private GoogleMap mMapTabMap;
    private Context mContext;
    private String mUsername;
    private SharedPreferences mSharedPreferences;
    private DatabaseHandler mdb;
    private PendingIntent mGeofencePendingIntent;

    //Uses the context from the MapTab so database handler can be used
    public GeofenceBuilder(GoogleMap map, Context context) {
        FacebookSdk.sdkInitialize(context);
        this.mMapTabMap = map;
        this.mContext = context;
        mdb = new DatabaseHandler(mContext);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        mSharedPreferences = context.getSharedPreferences("Login", Context.MODE_PRIVATE);

        //Check if logged in with facebook or not
        if (accessToken != null) {
            mUsername = accessToken.getUserId();
        } else {
            mUsername = mSharedPreferences.getString("USERNAME", "Error");
        }
        mAllFetchedGeofenceObjects = mdb.getGeofenceList(mUsername);
    }

    public List<Geofence> buildGeofences() {
        List<Geofence> Geofences = new ArrayList<Geofence>();
        //Creates a geofence for all stored points
        for (GeofenceObject geofenceObject : mAllFetchedGeofenceObjects) {
            Geofences.add(new Geofence.Builder()
                    .setRequestId(geofenceObject.getmPlace()) //Sers the request ID as the name of the geofence
                    .setCircularRegion(geofenceObject.getmLat(), geofenceObject.getmLong(), geofenceObject.getmRadius())
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT | Geofence.GEOFENCE_TRANSITION_DWELL)
                    .setLoiteringDelay(10 * 1000) //Activates after 10 seconds in a geofence
                    .build());
        }
        return Geofences;
    }

    public Void addGeofencesToMap() {
        for (GeofenceObject geofenceObject : mAllFetchedGeofenceObjects) {
            LatLng position = new LatLng(geofenceObject.getmLat(), geofenceObject.getmLong());

            mMapTabMap.addCircle(new CircleOptions()
                    .center(position)
                    .strokeColor(Color.BLUE)
                    .radius(geofenceObject.getmRadius()));

            mMapTabMap.addMarker(new MarkerOptions()
                    .position(position)
                    .title(geofenceObject.getmPlace())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))) //Colour
                    .setVisible(true);
        }
        return null;
    }

    //Specify geofences that need to be monitored and how events are triggered
    public GeofencingRequest getGeofenceRequests(List<Geofence> geofenceList) {
        GeofencingRequest.Builder request = new GeofencingRequest.Builder();
        request.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        request.addGeofences(geofenceList);
        return request.build();
    }

    //Used to trigger interactions  with the geofences
    public PendingIntent getGeofencingPendingIntent() {
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(mContext, GeofenceInteractionService.class);
        return PendingIntent.getService(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    //Adds all of the Geofences and their intent to a location request
    public void addGeofenceInteractions(GoogleApiClient googleApiClient) {
        if (googleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.GeofencingApi.addGeofences(
                        googleApiClient,
                        getGeofenceRequests(buildGeofences()),
                        getGeofencingPendingIntent()).setResultCallback(this);
            }
        }
    }

    //Used as a callback method when addGeofences request is sent
    @Override
    public void onResult(Status status) {
        Log.d("Adding Geofences", status.toString());
    }
}
