package com.aaron.geofencingapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    public DatabaseHandler(Context context) {
        //Constructor where super(context, databaseName, null, Database version)
        super(context, "appData", null, 22);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        //Used to create database for user information
        String userInfoTable = "CREATE TABLE userInformation(name TEXT, username TEXT PRIMARY KEY, password TEXT, profilePicture)";
        String settings = "CREATE TABLE userSettings(username TEXT PRIMARY KEY, geofenceOnMap TEXT, notifications TEXT)";
        String geofencePlaces = "CREATE TABLE geofencePlaces(place TEXT PRIMARY KEY, lat REAL, long REAL, radius REAL, addedByDefault INTEGER, username TEXT)";
        String interactions = "CREATE TABLE interactions(username TEXT, place TEXT, time TEXT, googlePlaceName TEXT, googlePlaceId TEXT)";
        try {
            database.execSQL(userInfoTable);
            database.execSQL(settings);
            database.execSQL(interactions);
            database.execSQL(geofencePlaces);
            populateGeofencePlaces(database);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //When the database version is updated, the old tables are dropped and the new ones are made
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + "userInformation");
        database.execSQL("DROP TABLE IF EXISTS " + "userSettings");
        database.execSQL("DROP TABLE IF EXISTS " + "geofencePlaces");
        database.execSQL("DROP TABLE IF EXISTS " + "interactions");
        onCreate(database);
    }

    //When the database version is downgraded, the old tables are dropped and the new ones are made
    @Override
    public void onDowngrade(SQLiteDatabase database, int oldVersion, int newVersion){
        database.execSQL("DROP TABLE IF EXISTS " + "userInformation");
        database.execSQL("DROP TABLE IF EXISTS " + "userSettings");
        database.execSQL("DROP TABLE IF EXISTS " + "geofencePlaces");
        database.execSQL("DROP TABLE IF EXISTS " + "interactions");
        onCreate(database);
    }

    //Used to log an interactionObject
    public boolean addInteraction(InteractionObject interactionObject) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues interactionValues = new ContentValues();
        interactionValues.put("username", interactionObject.getmUsername());
        interactionValues.put("place", interactionObject.getmPlace());
        interactionValues.put("time", interactionObject.getmTime());
        interactionValues.put("googlePlaceName", interactionObject.getmGooglePlaceName());
        interactionValues.put("googlePlaceId", interactionObject.getmGooglePlaceId());

        long newRow = database.insert("interactions", null, interactionValues);
        try {
            return newRow > 0;
        } finally {
            database.close();
        }
    }

    //Used to add a geofence to the database
    public boolean addGeofence(GeofenceObject geofence) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues geofenceValues = new ContentValues();
        geofenceValues.put("place", geofence.getmPlace());
        geofenceValues.put("lat", geofence.getmLat());
        geofenceValues.put("long", geofence.getmLong());
        geofenceValues.put("radius", geofence.getmRadius());
        geofenceValues.put("addedByDefault", 0);
        geofenceValues.put("username", geofence.getmUsername());

        long newRow = database.insert("geofencePlaces", null, geofenceValues);
        try {
            return newRow > 0;
        } finally {
            database.close();
        }
    }

    //Used to add a userObject to the database after registration
    public Boolean addUser(UserObject userObject) {
        Boolean check1 = false;
        Boolean check2 = false;

        //Gets data in write mode
        SQLiteDatabase database = this.getWritableDatabase();

        //Gets values for database row
        ContentValues values = new ContentValues();
        values.put("name", userObject.getmName());
        values.put("username", userObject.getmUsername());
        values.put("password", userObject.getmPassword());
        values.put("profilePicture", userObject.getmProfilePicture());

        //Get values for settings row
        ContentValues settings = new ContentValues();
        settings.put("username", userObject.getmUsername());
        settings.put("geofenceOnMap", "1");
        settings.put("notifications", "1");

        //Adds row to selected table
        long newRow = database.insert("userInformation", "profilePicture", values);
        if (newRow > 0) {
            Log.d("Insert userInformation", "Successful");
            check1 = true;
        } else
            Log.e("Insert userInformation", "Failed");

        long newSettingsRow = database.insert("userSettings", null, settings);
        if (newSettingsRow > 0) {
            Log.d("Insert userSettings", "Successful");
            check2 = true;
        } else
            Log.e("Insert userSettings", "Failed");

        try {
            return check1 && check2;
        } finally {
            //Closes the database when it has been used
            database.close();
        }
    }

    public void addFacebookUserSettings(String facebookID) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues settings = new ContentValues();
        settings.put("username", facebookID);
        settings.put("geofenceOnMap", "1");
        settings.put("notifications", "1");

        long newSettingsRow = database.insert("userSettings", null, settings);
        if (newSettingsRow > 0)
            Log.d("Insert userSettings", "Successful");
        else
            Log.e("Insert userSettings", "Failed");

        database.close();
    }

    //Used to check if a login exists based on information input on the login screen
    public Boolean checkLoginDetails(String username, String password) {
        SQLiteDatabase database = this.getReadableDatabase();
        String queryLogin = "SELECT * FROM userInformation WHERE username = '" + username + "' AND password = '" + password + "'";
        Cursor validLoginCursor = database.rawQuery(queryLogin, null);
        //Returns true if the cursor is not empty, this indicates that a login has been found
        try {
            return validLoginCursor.moveToFirst();
        } finally {
            validLoginCursor.close();
            database.close();
        }
    }

    //Used to see if a column cell value already exists in the user database
    public Boolean checkValueExists(String table, String field, String value) {
        SQLiteDatabase database = this.getReadableDatabase();
        String queryField = "SELECT * FROM " + table + " WHERE " + field + " = '" + value + "'";
        Cursor valueExistCursor = database.rawQuery(queryField, null);
        try {
            return valueExistCursor.moveToFirst();
        } finally {
            valueExistCursor.close();
            database.close();
        }
    }

    //Returns a string value based on the arguments passed to the method
    public String getStringValue(String table, String username, String value) {
        SQLiteDatabase database = this.getReadableDatabase();
        String queryField = "SELECT * FROM " + table + " WHERE username = '" + username + "'";
        Cursor getStringValue = database.rawQuery(queryField, null);
        try {
            while (getStringValue.moveToNext()) {
                return getStringValue.getString(getStringValue.getColumnIndex(value));
            }
        } finally {
            getStringValue.close();
            database.close();
        }
        return null;
    }

    public String getFenceStringValue(String place, String value) {
        SQLiteDatabase database = this.getReadableDatabase();
        String queryField = "SELECT " + value + " FROM geofencePlaces WHERE place = '" + place + "'";
        Cursor getStringValue = database.rawQuery(queryField, null);
        try {
            while (getStringValue.moveToNext()) {
                return getStringValue.getString(getStringValue.getColumnIndex(value));
            }
        } finally {
            getStringValue.close();
            database.close();
        }
        return null;
    }

    //Updates a string value based off username
    public void updateStringValue(String table, String username, String field, String value) {
        SQLiteDatabase database = this.getWritableDatabase();
        String updateQuery = "UPDATE " + table + " SET " + field + " = '" + value + "' WHERE username = '" + username + "'";
        try {
            database.execSQL(updateQuery);
            Log.d("UpdateStringValue", "Successful");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("updateStringValue", "Error updating string value");
        } finally {
            database.close();
        }
    }

    //Returns all known interactions as a object list
    public List<InteractionObject> getInteractions(String username) {
        List<InteractionObject> interactionObjectList = new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();
        String queryInteraction = "SELECT * FROM interactions WHERE username = '" + username + "'";
        Cursor getInteraction = database.rawQuery(queryInteraction, null);
        try {
            //Loops goes through all the interactions for the user
            while (getInteraction.moveToNext()) {
                //Creates an interactionObject object to add to the list
                InteractionObject interactionObject = new InteractionObject(
                        username,
                        getInteraction.getString(getInteraction.getColumnIndex("place")),
                        getInteraction.getString(getInteraction.getColumnIndex("time")),
                        getInteraction.getString(getInteraction.getColumnIndex("googlePlaceName")),
                        getInteraction.getString(getInteraction.getColumnIndex("googlePlaceId"))
                );
                interactionObjectList.add(interactionObject);
            }
        } finally {
            getInteraction.close();
            database.close();
        }
        return interactionObjectList;
    }

    //Returns all known geofences as an object list
    public List<GeofenceObject> getGeofenceList(String username) {
        List<GeofenceObject> geofenceObjectList = new ArrayList<GeofenceObject>();
        SQLiteDatabase database = this.getReadableDatabase();
        String queryGeofences = "SELECT * FROM geofencePlaces WHERE username = '" + username + "' OR addedByDefault = '1'";
        Cursor getGeofence = database.rawQuery(queryGeofences, null);
        try {
            while (getGeofence.moveToNext()) {
                GeofenceObject geofenceObject = new GeofenceObject(
                        getGeofence.getString(getGeofence.getColumnIndex("place")),
                        getGeofence.getDouble(getGeofence.getColumnIndex("lat")),
                        getGeofence.getDouble(getGeofence.getColumnIndex("long")),
                        getGeofence.getFloat(getGeofence.getColumnIndex("radius")),
                        getGeofence.getString(getGeofence.getColumnIndex("username"))
                );
                geofenceObjectList.add(geofenceObject);
            }
        } finally {
            getGeofence.close();
            database.close();
        }
        return geofenceObjectList;
    }

    //Deleted a chosen geofence
    public void deleteInteraction(String place, String dateAndTime) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("interactions", "place = '" + place + "' AND time = '" + dateAndTime + "'", null);
        database.close();
    }

    //Deleted a chosen geofence
    public void deleteGeofence(String geofenceName, String username) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("geofencePlaces", "place = '" + geofenceName + "' AND username = '" + username + "'", null);
        database.close();
    }

    //Deleted all of the none default geofences
    public void deleteAllGeofence(String username) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("geofencePlaces", "addedByDefault = '0' AND username = '" + username + "'", null);
        database.close();
    }

    //Used to see if a goefence exists for a specific account
    public Boolean checkGeofenceExists(String place, String username) {
        SQLiteDatabase database = this.getReadableDatabase();
        String queryField = "SELECT * FROM geofencePlaces WHERE place = '" + place + "' AND username = '" + username + "'";
        Cursor valueExistCursor = database.rawQuery(queryField, null);
        try {
            return valueExistCursor.moveToFirst();
        } finally {
            valueExistCursor.close();
            database.close();
        }
    }

    //Used to populate the default geofence database
    //TODO find a more efficient way of building multiple geofence datas
    public void populateGeofencePlaces(SQLiteDatabase database) {
        database.beginTransaction();

        //Coventry cathedral geofence
        ContentValues coventryCathedral = new ContentValues();
        coventryCathedral.put("place", "coventryCathedral");
        coventryCathedral.put("lat", 52.408584);
        coventryCathedral.put("long", -1.507023);
        coventryCathedral.put("radius", 60);
        coventryCathedral.put("addedByDefault", 1);

        //War memorial park geofence
        ContentValues memorialPark = new ContentValues();
        memorialPark.put("place", "memorialPark");
        memorialPark.put("lat", 52.391832);
        memorialPark.put("long", -1.522787);
        memorialPark.put("radius", 400);
        memorialPark.put("addedByDefault", 1);

        //Herbert Art Gallery park geofence
        ContentValues herbertArtGallery = new ContentValues();
        herbertArtGallery.put("place", "herbertArtGallery");
        herbertArtGallery.put("lat", 52.407378);
        herbertArtGallery.put("long", -1.506241);
        herbertArtGallery.put("radius", 50);
        herbertArtGallery.put("addedByDefault", 1);

        //Transport museum geofence
        ContentValues transportMuseum = new ContentValues();
        transportMuseum.put("place", "transportMuseum");
        transportMuseum.put("lat", 52.411215);
        transportMuseum.put("long", -1.509420);
        transportMuseum.put("radius", 50);
        transportMuseum.put("addedByDefault", 1);

        //Planet Ice geofence
        ContentValues planetIce = new ContentValues();
        planetIce.put("place", "planetIce");
        planetIce.put("lat", 52.406767);
        planetIce.put("long", -1.518179);
        planetIce.put("radius", 50);
        planetIce.put("addedByDefault", 1);

        //Belgrade Theatre geofence
        ContentValues belgradeTheatre = new ContentValues();
        belgradeTheatre.put("place", "belgradeTheatre");
        belgradeTheatre.put("lat", 52.409818);
        belgradeTheatre.put("long", -1.514383);
        belgradeTheatre.put("radius", 40);
        belgradeTheatre.put("addedByDefault", 1);

        //Coombe Abbey geofence
        ContentValues coombeAbbey = new ContentValues();
        coombeAbbey.put("place", "coombeAbbey");
        coombeAbbey.put("lat", 52.414481);
        coombeAbbey.put("long", -1.408251);
        coombeAbbey.put("radius", 600);
        coombeAbbey.put("addedByDefault", 1);

        //Fargo Village geofence
        ContentValues fargoVillage = new ContentValues();
        fargoVillage.put("place", "fargoVillage");
        fargoVillage.put("lat", 52.407651);
        fargoVillage.put("long", -1.493736);
        fargoVillage.put("radius", 40);
        fargoVillage.put("addedByDefault", 1);

        //Ricoh Arena geofence
        ContentValues ricohArena = new ContentValues();
        ricohArena.put("place", "ricohArena");
        ricohArena.put("lat", 52.448124);
        ricohArena.put("long", -1.495621);
        ricohArena.put("radius", 300);
        ricohArena.put("addedByDefault", 1);

        //St Marys Guildhall geofence
        ContentValues stMarysGuildhall = new ContentValues();
        stMarysGuildhall.put("place", "stMarysGuildhall");
        stMarysGuildhall.put("lat", 52.407734);
        stMarysGuildhall.put("long", -1.507731);
        stMarysGuildhall.put("radius", 30);
        stMarysGuildhall.put("addedByDefault", 1);

        //Adding geofence suggested by people in the survey
        //Allesley Park geofence
        ContentValues allesleyPark = new ContentValues();
        allesleyPark.put("place", "allesleyPark");
        allesleyPark.put("lat", 52.420589);
        allesleyPark.put("long", -1.564705);
        allesleyPark.put("radius", 400);
        allesleyPark.put("addedByDefault", 1);

        //Brandon Marsh geofence
        ContentValues brandonMarsh = new ContentValues();
        brandonMarsh.put("place", "brandonMarsh");
        brandonMarsh.put("lat", 52.375494);
        brandonMarsh.put("long", -1.431313);
        brandonMarsh.put("radius", 700);
        brandonMarsh.put("addedByDefault", 1);

        //Canal Basin geofence
        ContentValues canalBasin = new ContentValues();
        canalBasin.put("place", "canalBasin");
        canalBasin.put("lat", 52.413403);
        canalBasin.put("long", -1.511714);
        canalBasin.put("radius", 60);
        canalBasin.put("addedByDefault", 1);

        //add Content values to database
        database.insert("geofencePlaces", null, coventryCathedral);
        database.insert("geofencePlaces", null, memorialPark);
        database.insert("geofencePlaces", null, herbertArtGallery);
        database.insert("geofencePlaces", null, transportMuseum);
        database.insert("geofencePlaces", null, planetIce);
        database.insert("geofencePlaces", null, belgradeTheatre);
        database.insert("geofencePlaces", null, coombeAbbey);
        database.insert("geofencePlaces", null, fargoVillage);
        database.insert("geofencePlaces", null, ricohArena);
        database.insert("geofencePlaces", null, stMarysGuildhall);
        //Suggested by users
        database.insert("geofencePlaces", null, allesleyPark);
        database.insert("geofencePlaces", null, brandonMarsh);
        database.insert("geofencePlaces", null, canalBasin);

        //Used to maintain entered data
        database.setTransactionSuccessful();
        database.endTransaction();
    }
}
