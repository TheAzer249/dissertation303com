package com.aaron.geofencingapp;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.List;
import java.util.Locale;

public class PlaceDetails extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks {
    private Menu mMenu;
    private Place mCurrentPlace;
    private String mPlaceStringId;
    private String mPlaceStringName;
    private HorizontalScrollView mPhotoScrollView;
    private GoogleApiClient mGoogleApiClient;
    private RatingBar mPlaceRating;
    private TextView mPlaceName;
    private TextView mWebsite;
    private TextView mAddress;
    private TextView mPlaceRatingText;
    private TextView mPhoneNumber;
    private TextView mPrice;
    private LatLng mCurrentLatLng;
    private LinearLayout mPhotoContainer;
    private Intent mPreviousIntent;
    private static int PLACE_PICKER = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_details);

        mPlaceRating = (RatingBar) findViewById(R.id.placeDetailsRating);
        mPlaceName = (TextView) findViewById(R.id.placeDetailsName);
        mPlaceRatingText = (TextView) findViewById(R.id.placeDetailsRatingText);
        mPhotoContainer = (LinearLayout) findViewById(R.id.photoContainer);
        mPhotoScrollView = (HorizontalScrollView) findViewById(R.id.photoScrollview);
        mAddress = (TextView) findViewById(R.id.placeDetailsAddress);
        mWebsite = (TextView) findViewById(R.id.placeDetailsWebsite);
        mPhoneNumber = (TextView) findViewById(R.id.placeDetailsPhone);
        mPrice = (TextView) findViewById(R.id.placeDetailsCost);
        //Get the previous intent, used for place ID and menu selection
        mPreviousIntent = getIntent();

        if (mGoogleApiClient == null) {
            //Get the first places ID from the intent so that when connected we can process the data
            if (mPreviousIntent != null) {
                mPlaceStringId = mPreviousIntent.getStringExtra("PlaceId");
            }

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Places.GeoDataApi.getPlaceById(mGoogleApiClient, mPlaceStringId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                            //get the place from the saved ID
                            mCurrentPlace = places.get(0);
                            populateFields();
                        }
                        places.release();
                    }
                });
    }

    //Self explaining, populates all fields
    public void populateFields() {
        new PlaceDetailsPhotoGetter(mPhotoContainer, mPlaceStringId, PlaceDetails.this, mPhotoScrollView).execute();
        mPlaceStringName = mCurrentPlace.getName().toString();
        mPlaceStringId = mCurrentPlace.getId();
        mCurrentLatLng = mCurrentPlace.getLatLng();
        //Name
        mPlaceName.setText(mPlaceStringName);
        //Rating
        Float rating = mCurrentPlace.getRating() * 10;
        if (rating <= 0) {
            mPlaceRatingText.setText(getText(R.string.no_reviews));
            mPlaceRating.setRating(0);
        } else {
            mPlaceRating.setRating(rating);
            mPlaceRatingText.setText(rating.toString());
        }
        //Address
        CharSequence address = mCurrentPlace.getAddress();
        if (address.length() > 0) {
            mAddress.setText(address);
        } else {
            mAddress.setText(getText(R.string.no_data));
        }
        //Phone
        CharSequence phone = mCurrentPlace.getPhoneNumber();
        if (phone.length() > 0) {
            mPhoneNumber.setText(phone);
        } else {
            mPhoneNumber.setText(getText(R.string.no_data));
        }
        //Website
        Uri website = mCurrentPlace.getWebsiteUri();
        if (website != null) {
            mWebsite.setText(Html.fromHtml(website.toString()));
        } else {
            mWebsite.setText(getText(R.string.no_data));
        }
        //Cost
        int cost = mCurrentPlace.getPriceLevel();
        if (cost == -1) {
            mPrice.setText(getText(R.string.no_data));
        } else if (cost == 0) {
            mPrice.setText(getText(R.string.very_low_cost));
            mPrice.setTextColor(Color.GREEN);
        } else if (cost == 1) {
            mPrice.setText(getText(R.string.low_cost));
            mPrice.setTextColor(Color.GREEN);
        } else if (cost == 2) {
            mPrice.setText(getText(R.string.medium_cost));
            mPrice.setTextColor(Color.YELLOW);
        } else if (cost == 3) {
            mPrice.setText(getText(R.string.high_cost));
            mPrice.setTextColor(Color.RED);
        } else if (cost == 4) {
            mPrice.setText(getText(R.string.very_high_cost));
            mPrice.setTextColor(Color.RED);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER) {
            if (resultCode == RESULT_OK) {
                mCurrentPlace = PlacePicker.getPlace(data, this);
                mPlaceStringId = mCurrentPlace.getId();
                //Clear previous place pictures and hide the view
                mPhotoContainer.removeAllViews();
                //Show both menu options as place picker has selected a place that is possibly not close to the user
                //But, the user might still want to select more places
                mMenu.clear();
                getMenuInflater().inflate(R.menu.place_menu, mMenu);
                //Hide photo bar as new place could have no photos
                mPhotoScrollView.setVisibility(View.GONE);
                //Repopulate based on new fields
                populateFields();
                Log.d("place", mPlaceStringId);
            }
        }
    }

    //Options menu used for navigation or searching for nearby places
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.place_navigate: {//Go to nearby place via google maps navigation
                //User wants to navigate to the new place from their current location
                Uri intentURI = Uri.parse("google.navigation:q=" + mCurrentLatLng.latitude + "," + mCurrentLatLng.longitude + "&mode=w");
                Intent googleMaps = new Intent(Intent.ACTION_VIEW, intentURI);
                //Sets the package to google maps package
                googleMaps.setPackage("com.google.android.apps.maps");
                startActivity(googleMaps);
                return true;
            }
            case R.id.place_nearby: { //Open a place picker to see nearby areas
                PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(intentBuilder.build(PlaceDetails.this), PLACE_PICKER);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Intent previousIntent = getIntent();
        Boolean fromNotification = previousIntent.getExtras().getBoolean("Notification");
        if (previousIntent != null) {
            if (fromNotification == true) { //If its a notification you only need the nearby
                getMenuInflater().inflate(R.menu.place_menu_just_nearby, menu);
            } else {//If its a interaction you only need to navigate
                getMenuInflater().inflate(R.menu.place_menu_just_navigate, menu);
            }
        }
        mMenu = menu;
        return true;
    }

    @Override
    public void onConnectionSuspended(int i) {
    }


    @Override
    public void onBackPressed() {
        Intent goToDrawerContainer = new Intent(this, DrawerContainer.class);
        goToDrawerContainer.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(goToDrawerContainer);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Disconnect from client on destroy
        mGoogleApiClient.disconnect();
    }
}
