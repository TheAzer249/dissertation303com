package com.aaron.geofencingapp;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.ColorUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

public class InteractionDetailsFragment extends Fragment {
    private DatabaseHandler mdb;
    private String mStringTime;
    private String mStringPlace;
    private String mStringGooglePlaceName;
    private String mStringGooglePlaceId;
    private TextView mTime;
    private TextView mPlace;
    private TextView mPlaceId;
    private String mUsername;
    private SharedPreferences mSharedPreferences;
    private ActionMode mActionMode;
    private LinearLayout mFragmentInteractionContainer;
    private AccessToken mAccessToken;
    private RelativeLayout mInteractionContainer;

    public InteractionDetailsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_interaction_details, container, false);
        mdb = new DatabaseHandler(getActivity());
        mTime = (TextView) v.findViewById(R.id.dateAndTime);
        mPlace = (TextView) v.findViewById(R.id.location);
        mPlaceId = (TextView) v.findViewById(R.id.googlePlaceId);
        mFragmentInteractionContainer = (LinearLayout) getActivity().findViewById(R.id.interactionContainer);
        mInteractionContainer = (RelativeLayout) v.findViewById(R.id.fragment_interaction_details);
        mSharedPreferences = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
        mAccessToken = AccessToken.getCurrentAccessToken();


        //Gets arguments from the fragment transaction and sets to fields
        Bundle data = getArguments();
        mStringTime = data.getString("time");
        mStringPlace = data.getString("place");
        mStringGooglePlaceName = data.getString("googlePlaceName");
        mStringGooglePlaceId = data.getString("googlePlaceId");
        mPlace.setText(mStringGooglePlaceName);
        mTime.setText(mStringTime);
        mPlaceId.setText(mStringGooglePlaceId);

        if (mAccessToken != null) {
            mUsername = mAccessToken.getUserId();
        } else {
            mUsername = mSharedPreferences.getString("USERNAME", "Error");
        }
        
        //Listener for a long click on an individual fragment layout
        mInteractionContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //If there is already a menu open, don't open another
                if (mActionMode != null) {
                    return false;
                }
                //Open contextual action menu
                mActionMode = getActivity().startActionMode(mActionModeCallback);
                return true;
            }
        });

        return v;
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            //Inflate the contextual action bar with the facebook_interaction_contextual menu if logged into facebook
            //else locate the local_interaction_contextual menu
            MenuInflater inflater = mode.getMenuInflater();
            if (mAccessToken != null) {
                inflater.inflate(R.menu.facebook_interaction_contextual, menu);
            } else {
                inflater.inflate(R.menu.local_interaction_contextual, menu);
            }
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.interaction_contextual_share: {//if the item is going to be shared
                    shareToFacebook();
                    mode.finish(); //Close the action mode bar because something has been selected
                    return true;
                }
                case R.id.interaction_info:{
                    Intent goToInfo = new Intent(getActivity(), PlaceDetails.class);
                    goToInfo.putExtra("PlaceId", mStringGooglePlaceId);
                    goToInfo.putExtra("Notification", false);
                    startActivity(goToInfo);
                    return true;
                }
                case R.id.interaction_delete: {
                    Log.d("Delete", "Delete has been pressed");
                    final Boolean[] delete = {true};
                    Snackbar.make(mFragmentInteractionContainer, R.string.interaction_delete, Snackbar.LENGTH_LONG)
                            .setAction(R.string.undo, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //Adds the entry back to the database as undo has been clicked and re-adds the fragment to the view
                                    Log.d("Not Deleted", "The user has undo the delete.");
                                    delete[0] = false;
                                }
                            })
                            .setCallback(new Snackbar.Callback() {
                                @Override
                                public void onDismissed(Snackbar snackbar, int event) {
                                    super.onDismissed(snackbar, event);
                                    if (delete[0]) {
                                        Log.d("Deleted", "The user deleted an interaction.");
                                        //If the user does not click undo, assume they want the fragment to be removed, so do so.
                                        InteractionDetailsFragment thisFragment = (InteractionDetailsFragment) getFragmentManager()
                                                .findFragmentByTag(mStringTime);
                                        getFragmentManager().beginTransaction().remove(thisFragment).commit();
                                        mdb.deleteInteraction(mStringPlace, mStringTime);
                                    }
                                }
                            })
                            .show();
                    mode.finish();
                    return true;
                }
                default: {
                    mode.finish();
                    return false;
                }
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            //When the action mode destroyed the value is returned to null to indicate no action mode is active
            mActionMode = null;
        }
    };

    public void shareToFacebook() {
        ShareDialog shareDialog = new ShareDialog(getActivity());
        String lat = mdb.getFenceStringValue(mStringPlace, "lat");
        String lang = mdb.getFenceStringValue(mStringPlace, "long");
        //Creates the basic dialog for the sharing mechanic
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("I interacted with a geofence!")
                    .setContentDescription("Today I visited " + mStringGooglePlaceName)
                    //used to make a URL map of the visited location
                    .setContentUrl(Uri.parse("https://www.google.com/maps/preview/@" +
                            lat + "," +
                            lang + ",17z"))
                    .build();
            shareDialog.show(linkContent);
        }
    }
}