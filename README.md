# Aaron Lapper - 303COM Dissertation Submission Code#

### Project Title ###

* A geofencing based mobile application that can be used to enhance tourists experience.

### Project Abstract ###

* Geofencing is still a developing technology and due to this its usage within applications is still limited, especially within the tourism app sector. Geofencing is a technology that allows for geographical boundaries to be programmed into an application using longitude and latitude positioning. The created boundary can then be interacted with in different ways, allowing for location based feedback on a mobile device specific to the interaction type. For interactions with geofences to occur, there needs to be a form of stable device position monitoring in place, this is most commonly done through the use of either global positioning system or a standard mobile cell signal. This paper looks at the creation process of an Android based mobile application designed to improve the tourist experience in Coventry by utilizing geofencing technology that’s integrated within Googles location services. Geofences are integrated into the application by creating virtual boundaries around the city of Coventry at key tourist locations found during research. Location listeners then allow for geofence interaction detection so that location based tourist information can be sent to the Android device in an experience enhancing way. Finally, user tests are carried out on the created application, allowing feedback of the implementation itself to be produced. Ultimately, this data contains information on how to use geofencing in an Android application to enhance a tourists experience whilst exploring cities such as Coventry.

### Contact information ###

* lappera@uni.coventry.ac.uk